import React, {useEffect, useState} from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import Contacts from 'react-native-contacts';
import {useIsFocused} from '@react-navigation/native';

const Home = ({route, navigation}) => {
  const isFocused = useIsFocused();
  const [contacts, setContacts] = useState([]);
  const loadContacts = () => {
    Contacts.getAll().then(contactsU => {
      setContacts(contactsU);
    });
  };
  useEffect(() => {
    loadContacts();
  }, [isFocused]);
  const search = text => {
    if (text === '' || text === null) {
      loadContacts();
    } else {
      Contacts.getContactsMatchingString(text).then(users => {
        setContacts(users);
      });
    }
  };
  return (
    <View style={styles.root}>
      <TextInput
        placeholder="Search"
        style={styles.search}
        placeholderTextColor="#ffffff4d"
        onChangeText={search}
      />
      <ScrollView style={styles.contactsList}>
        {contacts.map(contact => {
          return (
            <TouchableOpacity
              key={contact.recordID}
              style={styles.contact}
              // onPress={() => openContact(contact)}>
              onPress={() =>
                navigation.navigate('Contact', {recordID: contact.recordID})
              }>
              <Image
                style={styles.contactAvatar}
                source={{
                  uri: contact.hasThumbnail
                    ? contact.thumbnailPath
                    : 'https://aquaforall.org/viawater/files/default-user.png',
                }}
              />
              <View>
                <Text style={styles.contactName}>{contact.givenName}</Text>
                <Text style={styles.contactPhone}>
                  {contact.phoneNumbers[0].number}
                </Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
      <TouchableOpacity
        style={styles.plus}
        onPress={() => navigation.navigate('Contact', {})}>
        <Image
          style={styles.plusImage}
          source={{
            uri: 'https://cdn0.iconfinder.com/data/icons/very-basic-2-android-l-lollipop-icon-pack/24/plus-512.png',
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#0e1621',
  },
  plus: {
    borderRadius: 50,
    position: 'absolute',
    backgroundColor: '#5da8f7',
    bottom: 15,
    right: 15,
  },
  plusImage: {
    height: 50,
    width: 50,
  },
  search: {
    color: '#ddebff',
    margin: 15,
    padding: 5,
    paddingLeft: 15,
    paddingRight: 15,
    fontSize: 16,
    borderColor: '#ddebff',
    borderRadius: 20,
    borderWidth: 1,
  },
  contactsList: {
    marginLeft: 15,
    marginRight: 15,
  },
  contact: {
    backgroundColor: '#2b5278',
    marginBottom: 10,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    color: '#fff',
  },
  contactName: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#fff',
  },
  contactPhone: {
    fontSize: 16,
    color: '#fff',
  },
  contactAvatar: {
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 30,
    height: 50,
    width: 50,
  },
});
export default Home;
