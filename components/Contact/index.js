import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  TextInput,
  Button,
  Linking,
} from 'react-native';
import Contacts from 'react-native-contacts';
import {launchImageLibrary} from 'react-native-image-picker';

const Contact = ({route, navigation}) => {
  const [contact, setContact] = useState({});
  const [phone, setPhone] = useState('');
  const [name, setName] = useState('');
  const getContact = () => {
    const contactID = route.params.recordID;
    if (contactID) {
      Contacts.getContactById(contactID).then(user => {
        user.number = user.phoneNumbers[0].number;
        setContact(user);
        setPhone(user.number);
        setName(user.givenName);
      });
    }
  };
  const updateContact = () => {
    if (phone !== '' && name !== '') {
      Contacts.updateContact({
        ...contact,
        givenName: name,
        phoneNumbers: [{label: 'mobile', number: phone}],
      }).then(() => {
        navigation.goBack();
      });
    }
  };
  const createContact = () => {
    if (phone !== '' && name !== '') {
      Contacts.addContact({
        ...contact,
        givenName: name,
        phoneNumbers: [{label: 'mobile', number: phone}],
      }).then(() => {
        navigation.goBack();
      });
    }
  };
  const deleteContact = () => {
    Contacts.deleteContact({recordID: contact.recordID}).then(recordId => {
      navigation.goBack();
    });
  };
  const uploadImage = () => {
    const options = {includeBase64: true, saveToPhotos: true};
    launchImageLibrary(options, response => {
      Contacts.writePhotoToPath(contact.recordID, response.assets[0].uri)
        .then(r => {
          console.log(r);
        })
        .catch(er => {
          console.warn(er);
        });
    });
  };
  useEffect(() => {
    getContact();
  }, []);

  return (
    <View style={styles.root}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Text style={styles.headerButton}>Back</Text>
        </TouchableOpacity>
        {contact.recordID && (
          <TouchableOpacity onPress={() => updateContact()}>
            <Text style={styles.headerButton}>Save</Text>
          </TouchableOpacity>
        )}
        {!contact.recordID && (
          <TouchableOpacity onPress={() => createContact()}>
            <Text style={styles.headerButton}>Create</Text>
          </TouchableOpacity>
        )}
      </View>
      <TouchableOpacity onPress={uploadImage}>
        <Image
          style={styles.userAvatar}
          source={{
            uri: contact.hasThumbnail
              ? contact.thumbnailPath
              : 'https://aquaforall.org/viawater/files/default-user.png',
          }}
        />
      </TouchableOpacity>
      <View style={styles.info}>
        <Text style={styles.infoLabel}>{contact.thumbnailPath}</Text>
        <TextInput
          style={styles.infoInput}
          value={name}
          onChangeText={text => setName(text)}
        />
        <Text style={styles.infoLabel}>Phone</Text>
        <TextInput
          style={styles.infoInput}
          value={phone}
          onChangeText={text => setPhone(text)}
        />
      </View>
      {contact.recordID && (
        <View style={styles.buttons}>
          <Button
            onPress={() => {
              Linking.openURL(`tel:${phone}`);
            }}
            title="Call"
            color="green"
          />
          <Button
            title="Delete"
            color="red"
            onPress={() => {
              deleteContact();
            }}
          />
        </View>
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#0e1621',
    color: '#ffffff',
    alignItems: 'center',
  },
  buttons: {
    marginTop: 20,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
  },
  info: {
    width: '100%',
    padding: 15,
  },
  infoLabel: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 10,
    marginBottom: 5,
    marginTop: 5,
  },
  infoInput: {
    fontSize: 18,
    color: '#000',
    backgroundColor: '#a2c2ef',
    borderRadius: 10,
    paddingLeft: 10,
  },
  header: {
    width: '100%',
    height: 60,
    backgroundColor: '#63a5ff',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerButton: {
    fontSize: 18,
    fontWeight: 'bold',
    margin: 15,
  },
  userAvatar: {
    width: 130,
    height: 130,
    margin: 20,
    backgroundColor: '#fff',
    borderColor: '#fff',
    borderWidth: 2,
    padding: 10,
  },
});
export default Contact;
